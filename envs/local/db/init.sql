CREATE USER spa_skeleton WITH PASSWORD 'spa_skeleton';
ALTER ROLE spa_skeleton SET client_encoding TO 'utf8';
ALTER ROLE spa_skeleton SET default_transaction_isolation TO 'read committed';
ALTER ROLE spa_skeleton SET timezone TO 'UTC';
CREATE DATABASE spa_skeleton ENCODING 'utf8' OWNER spa_skeleton;
