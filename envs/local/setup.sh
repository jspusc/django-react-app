# this script is used to create .env file filled with necessary host data as
# a prerequisite for starting local environment

# map host user and group for Linux users (Docker (for Linux) user and group is not same on host and inside container)
current_user="$(id -u)"
current_group=$(id -g)

# get project dir name to prefix container names with it
project_root_dir_url="$(cd ../../; pwd)"
project_root_dir_name="${project_root_dir_url##*/}"

# get system info
sysname="$(uname -s)"

# act according to system info
# if local development machine is Mac
case "${sysname}" in
  Darwin*)
    if [ ! -f .env ]; then
      echo "PROJECT_PREFIX=${project_root_dir_name}" >> .env
      echo "USER_ID=1000" >> .env
      echo "GROUP_ID=1000" >> .env
    fi
    ;;
esac
# if local development machine is Linux
case "${sysname}" in
  Linux*)
    if [ ! -f .env ]; then
      echo "PROJECT_PREFIX=${project_root_dir_name}" >> .env
      echo "USER_ID=$current_user" >> .env
      echo "GROUP_ID=$current_group" >> .env
    fi
    ;;
esac