from django.urls import include, path
from rest_framework import routers
from .api_views.user_checkin_checkout_api_view import UserCheckinCheckoutViewSet

router = routers.DefaultRouter()
router.register(r'checkin-checkout', UserCheckinCheckoutViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
