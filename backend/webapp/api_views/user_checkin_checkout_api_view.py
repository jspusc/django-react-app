from ..serializers import UserCheckinCheckoutSerializer
from rest_framework.viewsets import ModelViewSet
from ..models import UserCheckinCheckout


class UserCheckinCheckoutViewSet(ModelViewSet):
    queryset = UserCheckinCheckout.objects.all()
    serializer_class = UserCheckinCheckoutSerializer

    # def list(self, request, *args, **kwargs):
    #     super().list(request, *args, **kwargs)
