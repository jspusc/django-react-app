from rest_framework import serializers
from .models import UserCheckinCheckout


class UserCheckinCheckoutSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserCheckinCheckout
        fields = '__all__'
