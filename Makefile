APP_NAME:= $(notdir $(CURDIR))

.PHONY: backend frontend


# local commands

# it will build image only if it does not exist
start: generate-local-env-file
	@echo "== Starting local environment for $(APP_NAME) project"
	@cd envs/local && docker-compose -p $(APP_NAME) up -d

# if any change to Dockerfiles exist
start-build: generate-local-env-file
	@echo "== Building images and starting local environment for $(APP_NAME) project"
	@cd envs/local && docker-compose -p $(APP_NAME) up --build

restart: generate-local-env-file
	@echo "== Restarting local environment for $(APP_NAME) project"
	@cd envs/local && docker-compose -p $(APP_NAME) down
	@cd envs/local && docker-compose -p $(APP_NAME) up

stop:
	@echo "== Stopping local environment for $(APP_NAME) project"
	@cd envs/local && docker-compose -p $(APP_NAME) down

stop-db:
	@echo "== Stopping local environment and deleting database for $(APP_NAME) project"
	@cd envs/local && docker-compose -p $(APP_NAME) down -v

stop-all:
	@echo "== Stopping local environment, deleting database and related images for $(APP_NAME) project"
	@cd envs/local && docker-compose -p $(APP_NAME) down -v --rm all

# get project name, current user and group
generate-local-env-file:
	@cd envs/local && bash ./setup.sh

new-app:
	@read -p "What will the new app be called: " app_name; \
	docker container exec $(APP_NAME)_backend python manage.py startapp $$app_name
	@echo "App created"

shell:
	@echo "== Starting Django shell for $(APP_NAME) project"
	@docker container exec -it $(APP_NAME)_backend python manage.py shell

migrations:
	@echo "== Creating migration file(s) for $(APP_NAME) project"
	@docker container exec $(APP_NAME)_backend python manage.py makemigrations

migrate:
	@echo "== Applying migrations for $(APP_NAME) project"
	@docker container exec $(APP_NAME)_backend python manage.py migrate

superuser:
	@echo "== Creating superuser for $(APP_NAME) project"
	@docker container exec -it $(APP_NAME)_backend python manage.py createsuperuser

collectstatic:
	@echo "== Collecting staticfiles for $(APP_NAME) project"
	@docker container exec $(APP_NAME)_backend python manage.py collectstatic

lint:
	@echo "== Starting backend style check for $(APP_NAME) project"
	@docker container exec $(APP_NAME)_backend flake8
	@echo "..."
	@echo "== Starting frontend style check for $(APP_NAME) project"
	@docker container exec $(APP_NAME)_frontend npx eslint src/**/*.js

psql:
	@echo "== Entering psql shell for $(APP_NAME) project"
	@echo "== Connecting to database"
	@docker container exec -it $(APP_NAME)_db psql -U postgres postgres

psql-bash:
	@echo "== Entering psql bash shell for $(APP_NAME) project"
	@echo "== Connecting to database"
	@docker container exec -it $(APP_NAME)_db bash

backend:
	@echo "== Entering backend container for $(APP_NAME) project"
	@docker container exec -it $(APP_NAME)_backend bash

backend-logs:
	@docker logs $(APP_NAME)_backend

frontend:
	@echo "== Entering frontend container for $(APP_NAME) project"
	@docker container exec -it $(APP_NAME)_frontend bash

frontend-logs:
	@docker logs $(APP_NAME)_frontend

style-check:
	@docker container exec -t ${APP_NAME}_backend bash -c "flake8"
	@docker container exec -t ${APP_NAME}_frontend bash -c "npx eslint --max-warnings 0 src/**/*.js"

db:
	@echo "== Entering postgres shell for ${APP_NAME} project"
	@echo "== Connecting to database"
	@docker container exec -it ${APP_NAME}_db psql -U ${APP_NAME} ${APP_NAME}

docker-list:
	@echo ""
	@echo "============ IMAGES =====================================================================================\
	================================================================================================================"
	@echo ""
	@docker image ls
	@echo ""
	@echo "============ NETWORKS ===================================================================================\
	================================================================================================================"
	@echo ""
	@docker network ls
	@echo ""
	@echo "============ VOLUMES ====================================================================================\
	================================================================================================================"
	@echo ""
	@docker volume ls
	@echo ""
	@echo "============ ALL CONTAINERS =============================================================================\
	================================================================================================================"
	@echo ""
	@docker container ls -a
	@echo ""
	@echo "============ RUNNING CONTAINERS =========================================================================\
	================================================================================================================"
	@echo ""
	@docker container ls
	@echo ""

clean:
	@docker system prune --volumes --force


# prod commands

build-images:
	@cd backend  && docker build -t $(APP_NAME)/backend:latest .
	@cd frontend && docker build -t $(APP_NAME)/frontend:latest .

lint-backend:
	@echo "== Starting python style check"
	@docker container run \
	  --rm -t --mount src=`pwd`/backend,target=/backend,type=bind \
	  $(APP_NAME)/backend:latest \
	  flake8

lint-frontend:
	@echo "== Starting React style check"
	@docker container run \
	  --rm -t --mount src=`pwd`/frontend/src,target=/frontend/src,type=bind \
	  $(APP_NAME)/frontend:latest \
	  npx eslint --max-warnings 0 src/**/*.js

start-production-env:
	@echo "== Starting production environment"
	@cd envs/production && PROJECT_PREFIX=$(APP_NAME) docker-compose -p $(APP_NAME)_production up -d